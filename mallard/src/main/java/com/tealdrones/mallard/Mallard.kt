package com.tealdrones.mallard

import android.util.Log
import com.tealdrones.mallard.controller.OnErrorListener
import com.tealdrones.mallard.controller.OnResponseListener
import com.tealdrones.mallard.controller.Quacker
import com.tealdrones.mallard.model.DeleteIncubator
import com.tealdrones.mallard.model.Propfind
import com.tealdrones.mallard.model.Quack
import com.tealdrones.mallard.model.QuackerHead

/**
 * Created by kodyvanry on 7/26/17
 */
class Mallard {

    var url: String? = null
        get() {
            return field ?: throw MallardException("URL never was set")
        }

    /**
     * Get a list of a directory in WebDav
     *
     * @param folder the folder inside of the WebDav server to run the PROPFIND on
     * @param onResponseListener the listener that will listen for when the response comes through
     * @param onErrorListener the listener that will be called if an error occurs
     */
    fun getList(folder: String, requestCode: Int, onResponseListener: OnResponseListener?, onErrorListener: OnErrorListener?) {
        val propfind = Propfind(folder)
        propfind.requestCode = requestCode
        propfind.onResponseListener = onResponseListener
        propfind.onErrorListener = onErrorListener
        Quacker().quack(propfind)
    }


    /**
     * Get a list of a directory in WebDav
     *
     * @param file the file on the WebDav server to run the DELETE on
     * @param onResponseListener the listener that will listen for when the response comes through
     * @param onErrorListener the listener that will be called if an error occurs
     */
    fun delete(requestCode: Int, onResponseListener: OnResponseListener?, onErrorListener: OnErrorListener?, file: String) {
        val delete = DeleteIncubator(this, file).hatch()
        delete.requestCode = requestCode
        delete.onResponseListener = onResponseListener
        delete.onErrorListener = onErrorListener
        Quacker().quack(delete)
    }

    /**
     * Get a list of a directory in WebDav
     *
     * @param file the file on the WebDav server to run the DELETE on
     * @param onResponseListener the listener that will listen for when the response comes through
     * @param onErrorListener the listener that will be called if an error occurs
     */
    fun delete(requestCode: Int, onResponseListener: OnResponseListener?, onErrorListener: OnErrorListener?, vararg files: String) {
        val quacks = ArrayList<Quack>()
        files.forEachIndexed { index, it ->
            val delete = DeleteIncubator(this, it).hatch()
            quacks.add(delete)
        }
        val quackerHead = QuackerHead(onResponseListener, onErrorListener, requestCode, *quacks.toTypedArray())
    }

    companion object {

        @JvmStatic var _instance : Mallard? = null
        @JvmStatic val instance: Mallard
            get() {
                _instance = _instance ?: Mallard()
                return _instance!!
            }

    }

}

class MallardException(val mallardMessage: String) : Exception() {
    override val message: String?
        get() = "MallardException : $mallardMessage"
}

open class Foo {
    override fun equals(other: Any?): Boolean {
        Log.d("Foo", "equals")
        return super.equals(other)
    }
}

class Bar : Foo() {
    override fun equals(other: Any?): Boolean {
        Log.d("Foo", "equals")
        return super.equals(other)
    }
}

class Baz : Foo() {
    override fun equals(other: Any?): Boolean {
        Log.d("Foo", "equals")
        return super.equals(other)
    }
}
