package com.tealdrones.mallard.model

import com.tealdrones.mallard.controller.QuackParser
import okhttp3.Response

/**
 * Created by kodyvanry on 8/7/17
 * <p>
 * A QuackResponse is a wrapper for an OkHttp response from WebDav
 * </p>
 *
 * @constructor creates an instance of QuackResponse
 */
class QuackResponse(val requestCode: Int, val response: Response?) {

    constructor(requestCode: Int, ducks: ArrayList<Duck>) : this(requestCode, null) {
        this.ducks.addAll(ducks)
    }

    val ducks = ArrayList<Duck>()
    val succesful = response?.isSuccessful ?: false
        @JvmName("isSuccesful") get

    init {
        if (succesful && response != null)
            ducks.addAll(QuackParser(response.body().string()).parse())
    }

}