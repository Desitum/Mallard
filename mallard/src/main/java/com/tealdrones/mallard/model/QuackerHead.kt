package com.tealdrones.mallard.model

import com.tealdrones.mallard.controller.OnErrorListener
import com.tealdrones.mallard.controller.OnResponseListener

/**
 * Created by kodyvanry on 7/26/17
 * <p>
 * [QuackerHead] is a class used to query the WebDav server for a directory listing
 * </p>
 * @property folder folder to get a listing of
 * @property properties all the custom properties you are searching for
 *
 * @see Quack
 */
class QuackerHead internal constructor(val onResponseListener: OnResponseListener?, val onErrorListener: OnErrorListener?, val requestCode: Int, vararg _quacks: Quack) : OnResponseListener, OnErrorListener {

    var error: Boolean = false
    var currentQuack: Int = 0
    var quacks = _quacks.asList()
    var responseDucks = ArrayList<Duck>()

    override fun onError(quackErrorResponse: QuackErrorResponse) {
        error = true
        onErrorListener?.onError(quackErrorResponse)
    }

    override fun onResponse(response: QuackResponse) {
        responseDucks.addAll(response.ducks)
        if (!hasNext()) {
            onResponseListener?.onResponse(QuackResponse(requestCode, responseDucks))
        }
    }

    init {
        quacks.forEach {
            it.requestCode = REQUEST_CODE
            it.onResponseListener = this
            it.onErrorListener = this
        }
    }

    fun hasNext() : Boolean {
        return !error && currentQuack < quacks.size
    }

    var REQUEST_CODE: Int = 0
        get() {
            field += 1
            return field
        }

}
