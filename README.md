# Mallard


![alt text](https://github.com/TealDrones/Mallard/blob/master/mallard.png?raw=true)

Mallard is a WebDav library for Android. Aimed at making an easy to use and fun interface for WebDav on Android.